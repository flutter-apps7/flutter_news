import 'dart:convert';

import 'package:app_news/constant/Strings.dart';
import 'package:app_news/theme.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';

import 'constant/constants.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _keyForm = GlobalKey<FormState>();
  bool _obscureText = true;
  String username, email, password;
  var value;

  void _togglePassword() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  register(BuildContext context) async {
    final form = _keyForm.currentState;
    if (form.validate()) {
      form.save();
      ProgressDialog progressDialog = ProgressDialog(context);
      progressDialog = ProgressDialog(context,
          type: ProgressDialogType.Normal, isDismissible: false, showLogs: false);
      progressDialog.style(
        message: "Process",
        progressWidget: Container(
          padding: EdgeInsets.all(16.0),
          child: CircularProgressIndicator(),
        ),
        insetAnimCurve: Curves.easeInOut,
        progressTextStyle: blackTextStyle.copyWith(fontSize: 12),
        messageTextStyle: regularTextStyle.copyWith(fontSize: 16),
      );

      var url = Uri.parse(BaseUrl.register);
      final response = await http.post(
        url,
        body: {
          "username": username,
          "email": email,
          "password": password,
        },
      );

      await progressDialog.show();
      Future.delayed(Duration(seconds: 3)).then((value) {
        progressDialog.hide().whenComplete(() {
          // parsing json response body
          final result = jsonDecode(response.body);
          value = result["value"];
          String message = result["message"];

          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Text(Strings.information),
              content: Text(message),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.pop(context, Strings.close);
                    if (value == 1) {
                      Navigator.pop(context);
                    }
                  },
                  child: Text(Strings.close),
                ),
              ],
            ),
          );
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: whiteColor,
        body: SingleChildScrollView(
          padding: EdgeInsets.only(
            left: 16,
            right: 16,
            top: 100,
          ),
          child: Form(
            key: _keyForm,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Center(
                  child: Text(
                    "Register",
                    style: blueTextStyle.copyWith(fontSize: 40),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  onSaved: (e) => username = e,
                  decoration: InputDecoration(
                    labelText: "Username",
                  ),
                  validator: (String value) {
                    if (value == null || value.isEmpty) {
                      return 'Username tidak boleh kosong';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  onSaved: (e) => email = e,
                  decoration: InputDecoration(
                    labelText: "Email",
                  ),
                  validator: (String value) {
                    if (value == null || value.isEmpty) {
                      return 'Email tidak boleh kosong';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  onSaved: (e) => password = e,
                  obscureText: _obscureText,
                  decoration: InputDecoration(
                    labelText: "Password",
                    suffixIcon: IconButton(
                      icon: Icon(_obscureText
                          ? Icons.visibility
                          : Icons.visibility_off),
                      onPressed: _togglePassword,
                    ),
                  ),
                  validator: (String value) {
                    if (value == null || value.isEmpty) {
                      return 'Password tidak boleh kosong';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 40,
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: ElevatedButton(
                        onPressed: () {
                          register(context);
                        },
                        child: Text("Daftar"),
                        style: ElevatedButton.styleFrom(
                            primary: primaryColor,
                            padding: EdgeInsets.all(8.0),
                            textStyle: blackTextStyle.copyWith(
                              fontSize: 16,
                            )),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
