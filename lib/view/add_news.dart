import 'dart:convert';
import 'dart:io';

import 'package:app_news/constant/Strings.dart';
import 'package:app_news/constant/constants.dart';
import 'package:app_news/theme.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddNews extends StatefulWidget {
  @override
  _AddNewsState createState() => _AddNewsState();
}

class _AddNewsState extends State<AddNews> {
  final _keyForm = GlobalKey<FormState>();
  String title, content, description, userId;
  File _imageFile;
  final picker = ImagePicker();

  Future _pilihGallery() async {
    var image = await picker.getImage(
        source: ImageSource.gallery, maxWidth: 1080, maxHeight: 1920);
    setState(() {
      if (image != null) {
        _imageFile = File(image.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Future getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      userId = pref.getString("userId");
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    var placeholder = Container(
      width: double.infinity,
      height: 150,
      child: Image.asset("assets/images/placeholder.png"),
    );

    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: whiteColor,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "Add News",
          style: whiteTextStyle,
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16),
          child: Form(
            key: _keyForm,
            child: Column(
              children: [
                TextFormField(
                  onSaved: (e) => title = e,
                  decoration: InputDecoration(
                    labelText: "Judul",
                  ),
                  validator: (String value) {
                    if (value == null || value.isEmpty) {
                      return 'Judul tidak boleh kosong';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  onSaved: (e) => content = e,
                  decoration: InputDecoration(
                    labelText: "Isi",
                  ),
                  validator: (String value) {
                    if (value == null || value.isEmpty) {
                      return 'Isi tidak boleh kosong';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  onSaved: (e) => description = e,
                  decoration: InputDecoration(
                    labelText: "Deskripsi",
                  ),
                  validator: (String value) {
                    if (value == null || value.isEmpty) {
                      return 'Deskripsi tidak boleh kosong';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                InkWell(
                  onTap: () {
                    _pilihGallery();
                  },
                  child: Container(
                    child: _imageFile == null
                        ? placeholder
                        : Image.file(
                            _imageFile,
                            fit: BoxFit.fill,
                          ),
                    width: double.infinity,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: ElevatedButton(
                        onPressed: () {
                          save(context);
                        },
                        child: Text("Simpan"),
                        style: ElevatedButton.styleFrom(
                            primary: primaryColor,
                            padding: EdgeInsets.all(8.0),
                            textStyle: blackTextStyle.copyWith(
                              fontSize: 16,
                            )),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future save(BuildContext context) async {
    final form = _keyForm.currentState;

    ProgressDialog progressDialog = ProgressDialog(context);
    progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: false);
    progressDialog.style(
      message: Strings.process,
      progressWidget: Container(
        padding: EdgeInsets.all(16.0),
        child: CircularProgressIndicator(),
      ),
      insetAnimCurve: Curves.easeInOut,
      progressTextStyle: blackTextStyle.copyWith(fontSize: 12),
      messageTextStyle: regularTextStyle.copyWith(fontSize: 16),
    );

    if (form.validate()) {
      form.save();
      try {
        var stream = new http.ByteStream(_imageFile.openRead());
        stream.cast();
        var length = await _imageFile.length();
        var uri = Uri.parse(BaseUrl.addNews);
        var request = http.MultipartRequest("POST", uri);

        var multipartFile = new http.MultipartFile('image', stream, length,
            filename: path.basename(_imageFile.path));

        // add file to multipart
        request.files.add(multipartFile);

        request.fields['title'] = title;
        request.fields['content'] = content;
        request.fields['description'] = description;
        request.fields['id_users'] = userId;

        var streamedResponse = await request.send();
        var response = await http.Response.fromStream(streamedResponse);

        await progressDialog.show();
        Future.delayed(Duration(seconds: 3)).then((value) {
          progressDialog.hide().whenComplete(() {
            final result = jsonDecode(response.body);
            String message = result["message"];
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: Text(Strings.information),
                content: Text(message),
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context, Strings.close);
                      if (response.statusCode > 2) {
                        Navigator.pop(context);
                      }
                    },
                    child: Text(Strings.close),
                  ),
                ],
              ),
            );
          });
        });
      } catch (e) {
        print("error $e");
      }
    }
  }
}
