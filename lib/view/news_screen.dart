import 'dart:convert';

import 'package:app_news/constant/constants.dart';
import 'package:app_news/models/news_model.dart';
import 'package:app_news/theme.dart';
import 'package:app_news/view/add_news.dart';
import 'package:app_news/widget/news_card.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class NewsScreen extends StatefulWidget {
  @override
  _NewsScreenState createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {
  final List<NewsModel> list = <NewsModel>[];
  var loading = false;

  Future _getNews() async {
    list.clear();
    setState(() {
      loading = true;
    });

    var url = Uri.parse(BaseUrl.listNews);
    final response = await http.get(url);

    if (response.contentLength == 2) {
    } else {
      final data = jsonDecode(response.body);
      data.forEach((api) {
        final item = new NewsModel(
            api["id_news"],
            api["image"],
            api["title"],
            api["content"],
            api["description"],
            api["date_news"],
            api["id_users"],
            api["username"]);

        list.add(item);
      });

      setState(() {
        loading = false;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getNews();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: whiteColor,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "News",
          style: whiteTextStyle,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AddNews(),
              ));
        },
        child: Icon(Icons.add),
      ),
      body: RefreshIndicator(
        onRefresh: () {
          _getNews();
        },
        child: loading
            ? Center(child: CircularProgressIndicator())
            : ListView.builder(
                padding: EdgeInsets.symmetric(vertical: 16, horizontal: 4),
                itemCount: list.length,
                itemBuilder: (context, index) {
                  final item = list[index];
                  final newsModel = new NewsModel(
                      item.idNews,
                      item.image,
                      item.title,
                      item.content,
                      item.description,
                      item.dateNews,
                      item.idUsers,
                      item.username);

                  return NewsCard(newsModel);
                },
              ),
      ),
    );
  }
}
