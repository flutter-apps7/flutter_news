import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

//color
Color primaryColor = Color(0xfff44336);
Color lightColor = Color(0xffff7961);
Color darkColor = Color(0xffba000d);
Color secondaryColor = Color(0xff651fff);
Color secondaryLightColor = Color(0xffa255ff);
Color secondaryDarkColor = Color(0xff0100ca);
Color blackColor = Color(0xff000000);
Color whiteColor = Color(0xffFFFFFF);
Color greyColor = Color(0xff82868E);
Color cardColor = Color(0xffF6F7F8);

//dimens
double edge = 24;

//Text Style
TextStyle blackTextStyle = GoogleFonts.poppins(
  fontWeight: FontWeight.w500,
  color: blackColor,
);

TextStyle whiteTextStyle = GoogleFonts.poppins(
  fontWeight: FontWeight.w500,
  color: whiteColor,
);

TextStyle greyTextStyle = GoogleFonts.poppins(
  fontWeight: FontWeight.w300,
  color: greyColor,
);

TextStyle blueTextStyle = GoogleFonts.montserrat(
  fontWeight: FontWeight.w500,
  color: primaryColor,
);

TextStyle secondaryTextStyle = GoogleFonts.poppins(
  fontWeight: FontWeight.w500,
  color: secondaryColor,
);

TextStyle regularTextStyle = GoogleFonts.poppins(
  fontWeight: FontWeight.w400,
  color: blackColor,
);

TextStyle moonseratMedium = TextStyle(
  fontFamily: "Moonserat Medium",
  color: whiteColor,
);

TextStyle moonseratRegular = TextStyle(
  fontFamily: "Moonserat Regular",
  color: whiteColor,
);

TextStyle cardTextStyle = TextStyle(
  fontFamily: "Moonserat Regular",
  color: Color.fromRGBO(63, 63, 63, 1),
  fontSize: 14,
);
