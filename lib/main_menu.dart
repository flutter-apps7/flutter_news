import 'package:app_news/theme.dart';
import 'package:app_news/view/news_screen.dart';
import 'package:app_news/view/profile_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainMenu extends StatefulWidget {
  final VoidCallback signOut;

  const MainMenu({this.signOut});

  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {
  String username = "", email = "";

  signOut() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text("Informasi"),
        content: Text("Apakah anda yakin ingin keluar aplikasi ?"),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pop(context, 'Tidak');
            },
            child: Text(
              'Tidak',
              style: blueTextStyle,
            ),
          ),
          TextButton(
            onPressed: () {
              setState(() {
                widget.signOut();
              });
              Navigator.pop(context, 'Ya');
            },
            child: Text('Ya'),
          ),
        ],
      ),
    );
  }

  getPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print(prefs.getString('username'));
    setState(() {
      username = prefs.getString('username');
      email = prefs.getString('email');
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: whiteColor,
      body: Stack(
        children: [
          Container(
            height: size.height * .3,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage("assets/images/top_header.png"),
            )),
          ),
          SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                children: [
                  Container(
                    height: 64,
                    margin: EdgeInsets.only(bottom: 20),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CircleAvatar(
                          backgroundImage: NetworkImage(
                              "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png"),
                          radius: 32,
                        ),
                        SizedBox(
                          width: 16,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "$username",
                              style: moonseratMedium.copyWith(fontSize: 20),
                            ),
                            Text("$email",
                                style: whiteTextStyle.copyWith(fontSize: 14)),
                          ],
                        ),
                        Spacer(),
                        IconButton(
                            icon: Icon(
                              Icons.exit_to_app,
                              color: whiteColor,
                            ),
                            onPressed: () {
                              signOut();
                            }),
                      ],
                    ),
                  ),
                  Expanded(
                    child: GridView.count(
                      crossAxisCount: 2,
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 10,
                      primary: false,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(
                              builder: (context) {
                                return ProfileScreen();
                              },
                            ));
                          },
                          child: Card(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                            elevation: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.network(
                                  "https://image.flaticon.com/icons/svg/1904/1904425.svg",
                                  height: 128,
                                ),
                                Text(
                                  "Personal Data",
                                  style: cardTextStyle,
                                ),
                              ],
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(
                              builder: (context) {
                                return NewsScreen();
                              },
                            ));
                          },
                          child: Card(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                            elevation: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.network(
                                  "https://image.flaticon.com/icons/svg/1904/1904221.svg",
                                  height: 128,
                                ),
                                Text(
                                  "News",
                                  style: cardTextStyle,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
