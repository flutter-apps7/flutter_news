import 'dart:convert';

import 'package:app_news/constant/Strings.dart';
import 'package:app_news/enum.dart';
import 'package:app_news/main_menu.dart';
import 'package:app_news/register_page.dart';
import 'package:app_news/theme.dart';
import 'package:app_news/widget/alert_dialog.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'constant/constants.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  String email, password;
  final _formKey = GlobalKey<FormState>();
  bool _obscureText = true;
  LoginStatus _loginStatus = LoginStatus.notSignin;
  var value;
  SharedPreferences prefs;

  check(BuildContext context) {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      login(context);
    }
  }

  // Toggles show password
  void _togglePassword() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  //login asynhronous to API
  login(BuildContext context) async {
    ProgressDialog progressDialog = ProgressDialog(context);
    progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: false);
    progressDialog.style(
      message: Strings.authentication,
      progressWidget: Container(
        padding: EdgeInsets.all(16.0),
        child: CircularProgressIndicator(),
      ),
      insetAnimCurve: Curves.easeInOut,
      progressTextStyle: blackTextStyle.copyWith(fontSize: 12),
      messageTextStyle: regularTextStyle.copyWith(fontSize: 16),
    );

    var url = Uri.parse(BaseUrl.login);
    final response = await http.post(
      url,
      body: {
        "email": email,
        "password": password,
      },
    );

    await progressDialog.show();
    Future.delayed(Duration(seconds: 3)).then((value) {
      progressDialog.hide().whenComplete(() {
        // parsing json response body
        final result = jsonDecode(response.body);
        int value = result["value"];
        String message = result["message"];
        if (value == 1) {
          setState(() {
            _loginStatus = LoginStatus.Signin;
            var user = {
              'value': result["value"],
              'userId': result["id_users"],
              'username': result["username"],
              'email': result["email"]
            };

            savePref(user);
          });
        } else {
          showDialog(
            context: context,
            builder: (context) => DynamicDialog(
              title: Strings.information,
              message: message,
            ),
          );
        }
      });
    });
  }

  //menyimpan data response value kedalam sharedpreference
  savePref(Map user) async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setInt("value", user["value"]);
      prefs.setString("userId", user["userId"]);
      prefs.setString("username", user["username"]);
      prefs.setString("email", user["email"]);
    });
  }

  //get data session dari sharedpref kemudian diberikan kondisi login status
  getPref() async {
    prefs = await SharedPreferences.getInstance();
    value = prefs.getInt('value');
    setState(() {
      _loginStatus = value == 1 ? LoginStatus.Signin : LoginStatus.notSignin;
    });
  }

  //init data login status
  @override
  void initState() {
    super.initState();
    getPref();
  }

  signOut() async {
    prefs = await SharedPreferences.getInstance();
    prefs.setInt("value", 0);
    setState(() {
      _loginStatus = LoginStatus.notSignin;
    });
  }

  @override
  Widget build(BuildContext context) {
    switch (_loginStatus) {
      case LoginStatus.notSignin:
        return SafeArea(
          bottom: false,
          child: Scaffold(
            backgroundColor: whiteColor,
            body: Form(
              key: _formKey,
              child: Padding(
                padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 100.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Center(
                      child: Text(
                        "Login",
                        style: blueTextStyle.copyWith(
                          fontSize: 40,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      onSaved: (e) => email = e,
                      decoration: InputDecoration(
                        labelText: "Email",
                      ),
                      validator: (String value) {
                        if (value == null || value.isEmpty) {
                          return 'Email tidak boleh kosong';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      onSaved: (e) => password = e,
                      obscureText: _obscureText,
                      decoration: InputDecoration(
                        labelText: "Password",
                        suffixIcon: IconButton(
                          icon: Icon(_obscureText
                              ? Icons.visibility
                              : Icons.visibility_off),
                          onPressed: _togglePassword,
                        ),
                      ),
                      validator: (String value) {
                        if (value == null || value.isEmpty) {
                          return 'Password tidak boleh kosong';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Expanded(
                          flex: 4,
                          child: ElevatedButton(
                            onPressed: () {
                              check(context);
                            },
                            child: Text("Login"),
                            style: ElevatedButton.styleFrom(
                                primary: primaryColor,
                                padding: EdgeInsets.all(8.0),
                                textStyle: blackTextStyle.copyWith(
                                  fontSize: 16,
                                )),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          flex: 1,
                          child: ElevatedButton(
                            onPressed: () {},
                            child: Icon(Icons.fingerprint),
                            style: ElevatedButton.styleFrom(
                              primary: primaryColor,
                              padding: EdgeInsets.all(8.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    InkWell(
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(
                            builder: (context) {
                              return RegisterPage();
                            },
                          ));
                        },
                        child: RichText(
                          text: TextSpan(
                            text: 'Belum punya akun ? ',
                            style: regularTextStyle,
                            children: <TextSpan>[
                              TextSpan(
                                  text: 'Daftar', style: secondaryTextStyle),
                            ],
                          ),
                        )),
                  ],
                ),
              ),
            ),
          ),
        );
        break;
      case LoginStatus.Signin:
        return MainMenu(signOut: signOut);
      default:
        return MainMenu(signOut: signOut);
    }
  }
}
