import 'package:app_news/constant/constants.dart';
import 'package:app_news/models/news_model.dart';
import 'package:app_news/theme.dart';
import 'package:flutter/material.dart';

class NewsCard extends StatelessWidget {
  final NewsModel newsModel;

  NewsCard(this.newsModel);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: Container(
        padding: EdgeInsets.all(8),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.network(
                  "${BaseUrl.IPAddress}/upload/${newsModel.image}",
                  width: 150,
                  height: 120,
                  fit: BoxFit.cover,
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        newsModel.title,
                        style: blackTextStyle.copyWith(fontSize: 18),
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Text(newsModel.dateNews,
                          style: regularTextStyle.copyWith(fontSize: 12)),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 5,
            ),
          ],
        ),
      ),
    );
  }
}

