import 'package:flutter/material.dart';

class DynamicDialog extends StatefulWidget {
  DynamicDialog({this.title, this.message});

  final String title;
  final String message;

  @override
  _DynamicDialogState createState() => _DynamicDialogState();
}

class _DynamicDialogState extends State<DynamicDialog> {
  String _title;
  String _message;

  @override
  void initState() {
    _title = widget.title;
    _message = widget.message;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(_title),
      content: Text(_message),
      actions: [
        TextButton(
          onPressed: () => Navigator.pop(context, 'Tutup'),
          child: const Text('Tutup'),
        ),
      ],
    );
  }
}
